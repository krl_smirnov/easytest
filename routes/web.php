<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')
     ->name('home');

Route::group(['prefix' => 'users'], function () {
    Route::get('/{user}/profile', 'UserController@show')
         ->name('profile');

    Route::group(['middleware' => 'admin'], function () {
        Route::patch('/{user}/ban', 'UserController@ban')
             ->name('user.ban');

        Route::patch('/{user}/unban', 'UserController@unban')
             ->name('user.unban');

        Route::delete('/{user}/destroy', 'UserController@destroy')
             ->name('user.destroy');
    });
});


Route::group(['prefix' => 'replies', 'middleware' => ['auth', 'verified']], function () {
    Route::post('/', 'ReplyController@store')
         ->name('reply.store');

    Route::delete('/{reply}', 'ReplyController@destroy')
         ->middleware('admin')
         ->name('reply.destroy');
});
