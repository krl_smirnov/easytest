<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReplyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can leave a reply.
     *
     * @param  User  $user
     *
     * @return bool
     */
    public function create(User $user)
    {
        return ! $user->isBanned();
    }
}
