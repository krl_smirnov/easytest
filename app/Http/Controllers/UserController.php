<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * Display a specified user profile.
     *
     * @param  User  $user
     *
     * @return Application|Factory|View
     */
    public function show(User $user)
    {
        $replies = $user->replies()->orderBy('created_at', 'desc')->paginate(config('limits.replies_per_page'));

        return view('users.profile', [
            'user' => $user,
            'replies' => $replies
        ]);
    }

    /**
     * Ban a specified user.
     *
     * @param  User  $user
     *
     * @return RedirectResponse
     */
    public function ban(User $user)
    {
        $user->ban();

        return back()->with(['success' => 'User has been banned!']);
    }

    /**
     * Unban a specified user.
     *
     * @param  User  $user
     *
     * @return RedirectResponse
     */
    public function unban(User $user)
    {
        $user->unban();

        return back()->with(['success' => 'User has been unbanned!']);
    }

    /**
     * Delete a specified user.
     *
     * @param  User  $user
     *
     * @return Application|RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect('/')->with(['success' => 'User has been deleted!']);
    }
}
