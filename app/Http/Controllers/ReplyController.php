<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReplyRequest;
use App\Reply;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class ReplyController extends Controller
{
    /**
     * Persist new reply.
     *
     * @param  CreateReplyRequest  $request
     *
     * @return Application|RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function store(CreateReplyRequest $request)
    {
        $this->authorize('create', Reply::class);

        $data = $request->validated();

        auth()->user()->createReply($data['body']);

        return redirect('/')->with(['success' => 'Reply has been added']);
    }

    /**
     * Delete a given reply.
     *
     * @param  Reply  $reply
     *
     * @return Application|RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(Reply $reply)
    {
        $reply->delete();

        return redirect('/')->with(['success' => 'Reply has been deleted']);
    }
}
