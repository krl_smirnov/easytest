<?php

namespace App\Http\Controllers;

use App\Reply;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $replies = Reply::orderBy('created_at', 'desc')->paginate(config('limits.replies_per_page'));

        return view('home', [
            'replies' => $replies
        ]);
    }
}
