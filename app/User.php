<?php

namespace App;

use App\Events\NewReply;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }


    /**
     * User belongs to a role
     *
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    /**
     * Determine whether the user is admin
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role_id == Role::ADMIN;
    }

    /**
     * User may have many relies.
     *
     * @return HasMany
     */
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    /**
     * Ban the user
     */
    public function ban()
    {
        $this->is_banned = true;
        $this->save();
    }

    /**
     * Unban the user
     */
    public function unban()
    {
        $this->is_banned = false;
        $this->save();
    }

    /**
     * Determine whether the user is admin
     *
     * @return mixed
     */
    public function isBanned()
    {
        return $this->is_banned;
    }


    /**
     * Create reply
     *
     * @param  string  $body
     *
     * @return Model
     */
    public function createReply(string $body)
    {
        $reply = $this->replies()->create([
            'body' => $body
        ]);

        event(new NewReply($reply));

        return $reply;
    }
}
