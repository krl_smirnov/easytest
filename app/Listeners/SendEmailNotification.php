<?php

namespace App\Listeners;

use App\Events\NewReply;
use App\Notifications\NewReplyNotification;
use App\User;

class SendEmailNotification
{
    /**
     * Handle the event.
     *
     * @param  NewReply  $event
     *
     * @return void
     */
    public function handle(NewReply $event)
    {
        User::whereKeyNot($event->reply->author->id)
            ->get()
            ->each
            ->notify(new NewReplyNotification($event->reply));
    }
}
