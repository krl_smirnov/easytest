# Easy-agency test project

## Requirements
PHP 7.3  
MySql  8.0.17  
composer 1.8.0  
npm 6.4.1  
NodeJs 10.15.0  
####OR  
Docker 19.03.8  
Docker-compose 1.25.5

## Installation

### Development Environment
* Run `docker-compose up -d`
* Install vendor libs: `docker exec -it easy_php composer install`
* Run `cp .env.example .env` end edit .env file manually
* Run migrations with setting default data: `docker exec -it easy_php php artisan migrate --seed`
* Generate key `docker exec -it easy_php php artisan key:generate`
* Run `docker exec -it easy_php npm install` to install node dependencies
* Run `docker exec -it easy_php npm run prod` to compile assets 

## Environments
* Debug settings:
  * APP_DEBUG - (true|false)
  * APP_ENV - system environment (dev|prod|local)
* Change site root
  * APP_URL - change to you own site url
* Application name:
    * APP_NAME - change name of application
* Database settings
  * DB_CONNECTION - type of database (sqlite|mysql|pgsql|sqlsrv)
  * DB_HOST - database url
  * DB_PORT - database connection port (mysql: 3306|pgsql: 5432|sqlsrv:1433)
  * DB_DATABASE - name of your database
  * DB_USERNAME - database username 
  * DB_PASSWORD - database user password
* Email settings
  * MAIL_MAILER - mail connection type  
  * MAIL_HOST - mail host  
  * MAIL_PORT - mail port  
  * MAIL_USERNAME - mail username  
  * MAIL_PASSWORD - mail password  
  * MAIL_ENCRYPTION - mail encryption  
  * MAIL_FROM_ADDRESS - from field in emails
