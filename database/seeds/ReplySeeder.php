<?php

use App\Reply;
use App\User;
use Illuminate\Database\Seeder;

class ReplySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $replies = factory(Reply::class, 50)->create();
        $users   = User::all();

        $replies->each(function ($reply) use ($users) {
            $reply->author()->attach(
                $users->random(rand(1))->pluck('id')->toArray()
            );
        });
    }
}
