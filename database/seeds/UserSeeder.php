<?php

use App\Reply;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create()->each(function ($user) {
            factory(Reply::class, 10)->create(['user_id' => $user->id]);
        });

        factory(User::class)->create([
            'email' => 'admin@mail.com',
            'password' => Hash::make('password'),
            'role_id' => Role::ADMIN
        ]);
    }
}
