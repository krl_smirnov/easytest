@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        @if(count($errors))
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <div class="row">
            <div class="col-md-12">
                @include('replies.list')

                @if(auth()->check() && !auth()->user()->isBanned())
                    @include('replies.create')
                @elseif(auth()->check() && auth()->user()->isBanned())
                    <h5 class="text-center">Sorry, you are banned and cannot leave a reply!</h5>
                @else
                    <h5 class="text-center"><a href="{{ route('login') }}">Login</a> to leave a reply</h5>
                @endif
            </div>
        </div>
    </div>
@endsection
