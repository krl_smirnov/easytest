@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        @if(count($errors))
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="profile-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-md-10">
                                <h3>{{ $user->name }}</h3>
                                <h6>Email: <a href="mailto:{{ $user->email }}">{{ $user->email }}</a></h6>
                            </div>
                            @if(auth()->check() && auth()->user()->isAdmin())
                                <div class="col-12 text-center col-md-2 text-md-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger dropdown-toggle"
                                                data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false">
                                            Action
                                        </button>
                                        <div class="dropdown-menu">
                                            @if($user->isBanned())
                                                <form action="{{ route('user.unban', ['user'=>$user]) }}" method="post">
                                                    @csrf
                                                    @method('patch')
                                                    <button class="dropdown-item"><i class="fas fa-ban mr-1"></i>Unban
                                                    </button>
                                                </form>
                                            @else
                                                <form action="{{ route('user.ban', ['user'=>$user]) }}" method="post">
                                                    @csrf
                                                    @method('patch')
                                                    <button class="dropdown-item"><i class="fas fa-ban mr-1"></i>Ban
                                                    </button>
                                                </form>
                                            @endif
                                            <form action="{{ route('user.destroy', ['user'=>$user]) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button class="dropdown-item"><i class="fas fa-trash-alt mr-1"></i>Delete
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                @include('replies.list')
            </div>
        </div>

    </div>

@endsection