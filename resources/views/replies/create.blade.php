<div class="card">
    <div class="card-body">
        <h5>Leave a reply</h5>
        <form action="{{ route('reply.store') }}" method="post">
            @csrf
            <div class="form-group">
                <textarea class="form-control" rows="5" name="body"
                          placeholder="Reply message">{{ old('body') }}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>