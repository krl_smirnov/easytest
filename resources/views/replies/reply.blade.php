<div class="card mt-3">
    <div class="card-header">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <div class="col-6">
                    <h5>Posted by: <a href="{{ route('profile', $reply->author) }}">{{ $reply->author->name }}</a>
                        <span class="d-block d-md-inline-block">{{ $reply->created_at->diffForHumans() }}</span>
                    </h5>
                </div>
                @if(auth()->check() && auth()->user()->isAdmin())
                    <div class="col-6 text-right">
                        <form action="{{ route('reply.destroy', ['reply'=>$reply]) }}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger"><i class="fas fa-trash"></i></button>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="card-body">
        <p>{{ $reply->body }}</p>
    </div>
</div>