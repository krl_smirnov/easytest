@forelse($replies as $reply)
    @include('replies.reply')
@empty
    <h5 class="text-center">No replies</h5>
@endforelse
@if($replies->count() != 0)
    <div class="mt-3">
        {{ $replies->links() }}
    </div>
@endif